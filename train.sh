#! /bin/bash

# set feature type to use
FEATURE_TYPE=HOG

pushd $(dirname ${0})

    echo "Training using ${FEATURE_TYPE} features..."

    # create output directories
    mkdir -p ./data/positives  # directory for positive training data
    mkdir -p ./model  # directrory for final model

    # generate data
    opencv_createsamples \
        -vec data/positives/training.vec \
        -img data/logo/muni2.png \
        -bg data/negatives.txt \
        -num 10000 \
        -randinv \
        -maxidev 100 \
        -maxxangle 0.1 \
        -maxyangle 0.1 \
        -maxzangle 0.1 \
        -w 64 \
        -h 64 


    pushd data  # OMFG, opencv.

        # train model
        opencv_traincascade \
            -data ../model \
            -vec positives/training.vec \
            -bg negatives.txt \
            -numPos 2000 \
            -numNeg 2000 \
            -numStages 10 \
            -precalcValBufSize 1024 \
            -precalcIdxBufSize 1024 \
            -featureType ${FEATURE_TYPE} \
            -w 64 \
            -h 64
    popd

popd