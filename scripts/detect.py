import os
import sys
import cv2
import argparse
import time
import multiprocessing
from PIL import Image, ImageDraw

parser = argparse.ArgumentParser(description="""Detect muni logos given a model
                                 and set of images.""")

parser.add_argument('-m', '--model', type=str, required=True,
                    help="""A CascadeClassifier model as trained by
                    opencv (cascade.xml). This can be based on HoG,
                    LBP, HAAR features, as supported by opencv.""")

parser.add_argument('-o', '--output-path', type=str, required=False,
                    help="""If specified, images will be copied to this
                    directory annotated with their detections for better
                    visualization.""")

parser.add_argument('-p', '--processes', type=int, default=None,
                    help="""The number of processes run in parallel.
                    Defaults to the number of processors.""")

parser.add_argument('files', nargs='+', type=str,
                   help="""A list of images to process.""")


PROCESS_GLOBALS = None


def time_me(fn):
    """
    Decorator to print out elapsed time to stderr.
    """
    def wrapped(*args, **kwargs):
        t0 = time.time()
        result = fn(*args, **kwargs)
        t1 = time.time()
        print >> sys.stderr, "Time elapsed: %.3fs" % (t1 - t0)
        return result
    return wrapped


def pool_process_init(*args):

    classifier = cv2.CascadeClassifier(args[0])
    output_path = args[1]

    # Instantiate classifier and set params
    global PROCESS_GLOBALS
    PROCESS_GLOBALS = dict(classifier=classifier, output_path=output_path)


def get_detections(filename):

    # get classifier instance for this process
    global PROCESS_GLOBALS
    classifier = PROCESS_GLOBALS['classifier']
    output_path = PROCESS_GLOBALS['output_path']

    # load image
    im = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
    detections = classifier.detectMultiScale(im)

    # optionally visualize output
    if output_path is not None:
        pil_im = Image.fromarray(im).convert("RGB")

        draw = ImageDraw.Draw(pil_im)
        for i, (x, y, w, h) in enumerate(detections):
            color = (0, 230, 0) if i == 0 else (230, 0, 0)
            draw.rectangle([x, y, x + w, y + h], outline=color)

        fn = os.path.splitext(os.path.basename(filename))[0] + ".png"
        pil_im.save(os.path.join(output_path, fn))

    return (filename, detections)


@time_me
def main(args):

    pool = multiprocessing.Pool(args.processes,
        initializer=pool_process_init,
        initargs=(args.model, args.output_path))

    results = pool.map(get_detections, args.files)

    for (filename, detections) in results:

        # construct output str
        temp = [filename, str(len(detections))]
        for detection in detections:
            temp.extend([str(i) for i in detection])

        print >> sys.stdout, "\t".join(temp)


if __name__ == "__main__":
    sys.exit(main(parser.parse_args()))
