import sys
import argparse


parser = argparse.ArgumentParser(description='Dedup a set of files of URLs.')
parser.add_argument('files', nargs='+', type=argparse.FileType('r'),
                   help="""An (ordered) list of files to dedup.
                   Files are deduped in order, line by line, against all
                   preceding files.
                   """)


def main(args):
    seen = set()
    count = 0
    for f_in in args.files:
        with open(f_in.name + ".unique", "w") as f_out:
            print >> sys.stderr, "Uniquing", f_in.name, "to", f_out.name
            for line in f_in:
                line = line.strip()
                if line not in seen:
                    print >> f_out, line
                else:
                    count += 1
                seen.add(line)
    print >> sys.stderr, "Removed %d duplicate line(s)" % count


if __name__ == "__main__":
    sys.exit(main(parser.parse_args()))
