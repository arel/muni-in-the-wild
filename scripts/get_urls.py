import os
import sys
import argparse
import flickrapi


parser = argparse.ArgumentParser(description='Get image URLs from Flickr.')
parser.add_argument('-n', '--n-per-year', type=int, default=1000,
                   help='Max number of urls to obtain per year.')
parser.add_argument('query', type=str,
                   help='The query to search.')


URL_TEMPLATE = "http://farm{farm}.staticflickr.com/{server}/{id}_{secret}_z.jpg"

# read secrets from environment
try:
    API_KEY = os.environ["FLICKR_API_KEY"]
    API_SECRET = os.environ["FLICKR_API_SECRET"]
except:
    print "Envorinment variables not set."
    sys.exit(1)


def search(query, n_per_year):

    # authenticate
    flickr = flickrapi.FlickrAPI(API_KEY, API_SECRET)
    flickr.authenticate_via_browser(perms="read")

    # perform search for one year at a time
    for year in range(2006, 2016):

        i = 0

        # walk all the photo results
        print >> sys.stderr, "Searching '%s' in year %d" % (query, year)
        for photo in flickr.walk(
                sort="relevance",
                text=query,
                min_taken_date="%d-01-01" % year,
                max_taken_date="%d-12-31" % year):

            # limit number of results
            i += 1
            if i > n_per_year:
                break

            if not i % 10:
                print >> sys.stderr, ".",

            yield URL_TEMPLATE.format(
                farm=photo.get("farm"),
                server=photo.get("server"),
                id=photo.get("id"),
                secret=photo.get("secret"))

        print >> sys.stderr, "done."


def main(args):
    for url in search(query=args.query, n_per_year=args.n_per_year):
        print >> sys.stdout, url


if __name__ == "__main__":
    sys.exit(main(parser.parse_args()))
