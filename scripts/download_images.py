import os
import re
import sys
import argparse
import hashlib
import urllib
import multiprocessing


parser = argparse.ArgumentParser(description='Download a set of files of URLs.')
parser.add_argument('-l', '--list-files', action='store_true',
                    help="List photo IDs from URLs, and then exit.")
parser.add_argument('-o', '--output-path', type=str, default="",
                    help="""Output path where images should be downloaded to.
                    """)
parser.add_argument('files', nargs='+', type=argparse.FileType('r'),
                    help="""Download images from URLs in a given list of files
                    """)

URL_RE = re.compile(r"^.*\.staticflickr\.com/[^/]+/(?P<pid>[^/_]+)_.+_.\.jpg")
CONTENT_TYPE_RE = re.compile(r"image/jpe?g", re.IGNORECASE)


def id_from_url(url):
    m = URL_RE.match(url)
    photo_id = m.group('pid') if m else hashlib.md5(url).hexdigest()[:8]
    return photo_id


def download_url(datum, content_type=CONTENT_TYPE_RE, min_size=8192):
    try:
        # skip files that already exist
        if os.path.exists(datum['out_fn']):
            return

        print >> sys.stderr, datum['i'], "Downloading", datum['out_fn']

        # attempt to download the image
        _, headers = urllib.urlretrieve(datum['url'], datum['out_fn'])

        # skip images that are not jpegs (for now)
        if not CONTENT_TYPE_RE.match(headers.get('content-type')):
            raise Exception("Invalid image type.")

        # skip images that are small
        if headers.get('content-length') < min_size:
            raise Exception("Insufficient image length.")

    except:
        print >> sys.stderr, "Skipping", datum['out_fn'], sys.exc_info()[1]
        if os.path.exists(datum['out_fn']):
            os.unlink(datum['out_fn'])


def main(args):

    # list of images to download
    data = []

    i = 0
    for f_in in args.files:
        print >> sys.stderr, "Reading", f_in.name
        for url in f_in:
            i += 1
            url = url.strip()
            pid = id_from_url(url)
            ext = os.path.splitext(url)[1]
            out_fn = os.path.join(args.output_path, pid + ext)
            data.append(dict(i=i, out_fn=out_fn, url=url))

    # if necessary, list files and urls then exit
    if args.list_files:
        for d in data:
            print >> sys.stdout, "\t".join([d['out_fn'], d['url']])
        return 0

    # download the images in parallel
    print >> sys.stderr, "Starting download of %d file(s)" % i
    pool = multiprocessing.Pool(5)
    pool.map(download_url, data)


if __name__ == "__main__":
    sys.exit(main(parser.parse_args()))
