# Muni detector

The goal of this project is to build a minimal but efficient logo detector using Python’s OpenCV library.
First choose any brand and find a single cropped image of that brand’s logo and
gather example training and testing data for this brand. 

**Problem:** Given N images perform classification and localization task for this logo.

 - Allow for different methods as an option e.g. method=“HOG”,  method=“SURF”, 
 - Utilitize the gpu for classification and localization
 - Speed up your code with multiprocessing and multithreading libraries

## Installing dependencies

This project depends on **OpenCV** (I used version 2.4.12).

On OSX, I install OpenCV using homebrew as such:

    $ pip install numpy                 # make sure python/numpy is installed
    $ brew install opencv --with-tbb    # on Intel platform TBB enables multicore

I use **virtualenv** to isolate python dependencies.

    # setup virtualenv
    $ pip install virtualenv
    $ virtualenv muni-env
    $ source muni-env/bin/activate

    # copy OpenCV library into virtualenv
    (muni-env)$ cp /usr/local/lib/python2.7/site-packages/cv* <path-to-venv>/lib/python2.7/site-packages

    # install python dependencies
    (muni-env)$ pip install -r requirements.txt  # (as usual)



## Downloading data

First, I obtained URLs from Flickr for images corresponding to
a partucular search. I wrote the script below to perform two
searches and retrieve the resulting URLs.

    # set up your Flickr API credentials (they are not checked into this repo!)
    export FLICKR_API_KEY=<your-key>
    export FLICKR_API_SECRET=<your-secret>

    python scripts/get_urls.py "boston" > data/urls/boston.ids  # I use this for negative images / background images
    python scripts/get_urls.py "muni logo" > data/urls/muni_logs.urls  # I use this as a test set at the very end

    # To be safe, I deduplicate the URLs so the files are disjoint:
    python scripts/dedup_urls.py data/urls/*.urls

The deduped URLs are checked into this repository. Next, these images must be downloaded for training. For this I wrote the following script:

    python scripts/download_images.py --output-path=data/images/ data/urls/*.urls

## Training

To train I wrote the following script. You can choose the feature type to train with (HOG, HAAR, LBP) by editing the
`FEATURE_TYPE` variable.

    ./train.sh

This script uses OpenCV's data augmentation tool to generate
positive example images from *a single* exemplar against a
variety of backgrounds. This is not ideal. In practice you should
have as many variations of the logo as possible. You can see
the limitation of using only one exemplar in my test detection
results (below). For instance, occluded logos are not detected.

![The Muni Logo used as single positive.](data/logo/muni2.png)

I crudely took this from the following image:

![The image the examplar logo came from.](data/samples/exemplar.jpg)


## Detecting logos

Logos are detected using the script `detect.py`. The file `test.txt` contains the held-out test set of images (the first 100
images listed in `data/urls/muni_logo.urls`):

    cd data
    cat test.txt | xargs python ../scripts/detect.py -m ../model/cascade.xml -o results


This script
takes as input a set of images, and outputs to *stdout* a
tab-separated table:

    filename    number_of_detections    x1  y1  w1  h2  ...

Additionally, if the `--output-path` argument is used, this
script will save copies of the input images, along with
rectangles of their detections.

 - the *green* rectangle represents the first detection (in OpenCV's ordering).
 - the remaining detections are in *red*.

### Multiprocessing

The `detect.py` by default uses a pool of workers the
size of the number of cores on your machine. You can change this using the `--processes` (or `-p`) argument.
For example:

    python scripts/detect.py -p 1 -m ../model/cascade.xml <path-to-your-images>/*.jpg  # took 14.988s for a set of images I had
    python scripts/detect.py -p 8 -m ../model/cascade.xml <path-to-your-images>/*.jpg  # took 3.867s for the same set of images

### GPU acceleration

I didn't have the chance to implement this, but OpenCV supports
[GPU accelleration](https://github.com/Itseez/opencv/blob/master/samples/gpu/cascadeclassifier.cpp) of the cascade classifier.

This functionality is not exposed in Python, but a C++
implementation could be called from [Python using the built-in *ctypes* module](http://stackoverflow.com/a/145649/2438538).

## Results

The trained model is checked into this repository at `model/cascade.xml`. Using this model and the 100 test images
listed at `data/test.txt`, yielded the following results:


### Example successes

![](data/samples/ok/253788920.png)
![](data/samples/ok/259824033.png)
![](data/samples/ok/2824642774.png)
![](data/samples/ok/2852291836.png)
![](data/samples/ok/3254356979.png)
![](data/samples/ok/3618956077.png)
![](data/samples/ok/5052340254.png)
![](data/samples/ok/5250417706.png)
![](data/samples/ok/4149849993.png)

### Example failures

![](data/samples/fail/1667570561.png)
![](data/samples/fail/3285296251.png)
![](data/samples/fail/4222734197.png)
![](data/samples/fail/4264856181.png)
![](data/samples/fail/5927150184.png)
![](data/samples/fail/733694821.png)


[See all 100 test detections](https://www.dropbox.com/sh/3h7tqe430iab1nq/AACp32KfuY9Jl967EQ_sTQWma?dl=0)

## Future improvments

This is a first pass at using OpenCV for muni-logo detection.
The detector is fast and shows promise, though it's still
very limited. Some ideas for improvments:
 
 - Use multiple example logos (more than one at least!).
   As evident in the results, a single image even if rotated
   and transformed to generate more data misses out on the
   many variations of transformations in the wild, such as
   occlusions or non-linear warping.
 - Improve training data synthesis. Even with a single examplar,
   the positive dataset could have been improved. For instance,
   instead of limiting positive images to affine transformations, as the OpenCV module seems to do, one could
   add more types of warping, artificial occlusions, or translucency to generate more representative positive examples.
 - Use convolutional network approach instead, such as [R-CNN detector](http://www.cs.berkeley.edu/~rbg/papers/r-cnn-cvpr.pdf).
 
 